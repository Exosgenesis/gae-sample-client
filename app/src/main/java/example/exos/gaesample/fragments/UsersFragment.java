package example.exos.gaesample.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import example.exos.gaesample.R;
import example.exos.gaesample.adapters.UserListAdapter;
import example.exos.gaesample.base.BaseApp;
import example.exos.gaesample.models.User;
import example.exos.gaesample.models.UsersList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class UsersFragment extends PlaceholderFragment implements Callback<UsersList> {

    public final static int INDEX = 2;

    private ListView listView;
    private TextView stub;

    private User[] users;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        root = (RelativeLayout) inflater.inflate(R.layout.fragment_users, container, false);

        listView = (ListView) root.findViewById(R.id.list_view);
        stub = (TextView) root.findViewById(R.id.empty_text);

        fetchData();
        return root;
    }

    @Override
    public void onCacheClear() {
        fetchData();
    }

    private void fetchData() {
        showHudLock(R.string.users_wait);
        BaseApp.getApi().getUsers(this);
    }

    private void onItemClick(int i) {
        Bundle args = new Bundle();
        args.putLong(UserFragment.USER_ID_ARG, users[i].getId());
        fragmentSelector.selectFragment(UserFragment.INDEX, args);
    }

    @Override
    public void success(UsersList usersList, Response response) {
        BaseApp.logI("SUCCESS CALL");
        try {
            hideHudLock();
            users = usersList.getItems();

            if (users.length == 0)
                return;

            listView.setAdapter(new UserListAdapter(users, getContext()));
            listView.setOnItemClickListener((av, v, i, l) -> onItemClick(i));
            listView.setVisibility(View.VISIBLE);
            stub.setVisibility(View.GONE);

        } catch (Exception e) {
        }
    }

    @Override
    public void failure(RetrofitError error) {
        try {
            hideHudLock();
            displayNetError(error);
        } catch (Exception ignored) {}
    }
}
