package example.exos.gaesample.fragments;

import android.os.Bundle;

import example.exos.gaesample.FragmentSelector;


public class FragmentFactory {

    public static PlaceholderFragment newInstance(int sectionNumber, Bundle bundle,
                                                  FragmentSelector selector)
    {

        PlaceholderFragment fragment = null;

        switch (sectionNumber) {
            case 1: fragment = new GetUserFragment();
                break;
            case 2: fragment = new UsersFragment();
                break;
            case 3: fragment = new TestGCMFragment();
                break;
            case 4: fragment = new UserFragment();
                break;
        }

        Bundle args = new Bundle();
        if (bundle != null)
            args.putAll(bundle);
        args.putInt(PlaceholderFragment.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        fragment.setFragmentSelector(selector);
        return fragment;
    }

}
