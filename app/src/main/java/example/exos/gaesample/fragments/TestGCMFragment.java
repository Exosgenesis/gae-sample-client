package example.exos.gaesample.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import example.exos.gaesample.R;
import example.exos.gaesample.base.BaseApp;
import example.exos.gaesample.models.GcmSenderTarget;
import example.exos.gaesample.services.RegistrationIntentService;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class TestGCMFragment extends PlaceholderFragment implements Callback<Void> {

    public final static int INDEX = 3;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String REG_ID_PREFS = "reg_id";

    private BroadcastReceiver mRegistrationBroadcastReceiver = new RegistrationReceiver();
    private Button goButt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        root = (RelativeLayout) inflater.inflate(R.layout.fragment_test_gcm, container, false);

        goButt = (Button) root.findViewById(R.id.go);
        goButt.setOnClickListener(x -> onGoPressed());
        goButt.setEnabled(false);

        showHudLock(R.string.test_gcm_register);

        if (checkPlayServices()) {
            Intent intent = new Intent(getContext(), RegistrationIntentService.class);
            getActivity().startService(intent);
        }

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                mRegistrationBroadcastReceiver,
                new IntentFilter(RegistrationIntentService.REGISTRATION_COMPLETE));
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(
                mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void onGoPressed() {
        showHudLock(R.string.test_gcm_wait);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        String regId = prefs.getString(REG_ID_PREFS, "");
        BaseApp.getApi().gcmTest(new GcmSenderTarget(regId), this);
    }

    @Override
    public void success(Void aVoid, Response response) {
        try {
            hideHudLock();
            alert(R.string.test_gcm_success);
        } catch (Exception ignored) {}
    }

    @Override
    public void failure(RetrofitError e) {
        try {
            hideHudLock();

            if (e.getResponse() != null && e.getResponse().getStatus() == 400)
                alertE(R.string.test_gcm_error_busy);
            else
                displayNetError(e);

        } catch (Exception ignored) {}
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(getContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            hideHudLock();

            if (apiAvailability.isUserResolvableError(resultCode)) {

                apiAvailability.getErrorDialog(getActivity(),
                                               resultCode,
                                               PLAY_SERVICES_RESOLUTION_REQUEST).show();

            } else {
                alertE(R.string.test_gcm_not_support);
            }

            return false;
        }
        return true;
    }

    private class RegistrationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            hideHudLock();
            SharedPreferences sharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(context);

            boolean sentToken = sharedPreferences
                    .getBoolean(RegistrationIntentService.SENT_TOKEN_TO_SERVER, false);

            goButt.setEnabled(sentToken);

            if (sentToken)
                Toast.makeText(getContext(), R.string.test_gcm_reg_success, Toast.LENGTH_SHORT)
                        .show();
            else
                alertE(R.string.test_gcm_reg_failure);
        }
    }
}
