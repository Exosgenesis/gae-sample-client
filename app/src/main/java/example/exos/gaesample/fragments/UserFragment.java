package example.exos.gaesample.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import example.exos.gaesample.R;
import example.exos.gaesample.base.BaseApp;
import example.exos.gaesample.models.ErrorResponse;
import example.exos.gaesample.models.User;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class UserFragment extends PlaceholderFragment implements Callback<User> {

    public final static int INDEX = 4;

    public final static String USER_ID_ARG = "user_id";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        root = (RelativeLayout) inflater.inflate(R.layout.fragment_user, container, false);
        fetchData();
        return root;
    }

    @Override
    public void onCacheClear() {
        fetchData();
    }

    private void fetchData() {
        showHudLock(R.string.user_wait);
        BaseApp.getApi().getUser(getArguments().getLong(USER_ID_ARG), this);
    }

    private void setText(int id, Object cnt) {
        if (cnt == null || String.valueOf(cnt).trim().equals(""))
            return;
        ((TextView)root.findViewById(id)).setText(String.valueOf(cnt));
    }

    @Override
    public void success(User user, Response response) {
        BaseApp.logI("SUCCESS CALL");
        try {
            hideHudLock();
            setText(R.id.user_id, user.getId());
            setText(R.id.user_name, user.getName());
            setText(R.id.user_desc, user.getDesc());

        } catch (Exception ignored) {
        }
    }

    @Override
    public void failure(RetrofitError e) {
        try {
            hideHudLock();

            if (e.getResponse() != null && e.getResponse().getStatus() == 404)
                alertE(R.string.user_404);
            else
                displayNetError(e);

        } catch (Exception ignored) {}
    }
}
