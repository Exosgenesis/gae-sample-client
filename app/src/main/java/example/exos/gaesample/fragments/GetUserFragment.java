package example.exos.gaesample.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import example.exos.gaesample.R;


public class GetUserFragment extends PlaceholderFragment {

    public final static int INDEX = 1;

    private EditText uId;
    private Button fetch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = (RelativeLayout) inflater.inflate(R.layout.fragment_get_user, container, false);

        uId = (EditText) root.findViewById(R.id.user_id);
        fetch = (Button) root.findViewById(R.id.fetch);
        fetch.setOnClickListener(v -> onFetchPressed());

        return root;
    }

    private void onFetchPressed() {
        try {
            long id = Long.valueOf(uId.getText().toString());
            Bundle args = new Bundle();
            args.putLong(UserFragment.USER_ID_ARG, id);
            fragmentSelector.selectFragment(UserFragment.INDEX, args);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
