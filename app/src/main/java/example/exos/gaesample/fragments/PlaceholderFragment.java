package example.exos.gaesample.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import example.exos.gaesample.FragmentSelector;
import example.exos.gaesample.MainActivity;
import example.exos.gaesample.R;
import example.exos.gaesample.models.ErrorResponse;
import retrofit.RetrofitError;

public class PlaceholderFragment extends Fragment {

    public static final String ARG_SECTION_NUMBER = "section_number";

    protected RelativeLayout root;
    protected FragmentSelector fragmentSelector;
    private View hud;

    public PlaceholderFragment() {
    }

    public void setFragmentSelector(FragmentSelector fragmentSelector) {
        this.fragmentSelector = fragmentSelector;
    }

    protected void showHudLock(int strId) {
        if (hud == null)
            hud = LayoutInflater.from(getActivity()).inflate(R.layout.hud_lock, null, false);

        ((TextView)hud.findViewById(R.id.msg)).setText(strId);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        if (hud.getParent() == null)
            root.addView(hud, params);
    }

    protected void hideHudLock() {
        if (hud != null && hud.getParent() != null)
            root.removeView(hud);
    }

    public void onCacheClear() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
    }

    protected void displayNetError(RetrofitError e) {
        ErrorResponse err = ((ErrorResponse) e.getBodyAs(ErrorResponse.class));
        alert(R.string.networк_error, err.getError().getMessage(), true);
    }

    protected void alert(int strId) {
        alert(strId, null, false);
    }

    protected void alertE(int strId) {
        alert(strId, null, true);
    }

    protected void alert(int msgId, String msg, boolean error) {
        String m = msgId != 0 ? getString(msgId) : "";
        if (msg != null) m += msg;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage(m)
                .setPositiveButton(android.R.string.ok, (d, i) -> d.dismiss());

        if (error)
            builder.setTitle(R.string.error);

        builder.create().show();
    }
}
