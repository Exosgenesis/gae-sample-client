package example.exos.gaesample.models;

import java.io.Serializable;

public class User implements Serializable {

    private Long id;
    private String name;
    private String desc;

    public User() {
    }

    public User(Long id, String name, String decs) {
        this.id = id;
        this.name = name;
        this.desc = decs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String decs) {
        this.desc = decs;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}