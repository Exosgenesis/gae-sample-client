package example.exos.gaesample.models;

import java.io.Serializable;

public class GcmSenderTarget implements Serializable {

    private String regId;

    public GcmSenderTarget() {
    }

    public GcmSenderTarget(String regId) {
        this.regId = regId;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    @Override
    public String toString() {
        return "GcmSenderTarget{" +
                "regId='" + regId + '\'' +
                '}';
    }
}
