package example.exos.gaesample.models;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private ErrorBody error;

    public ErrorResponse(){}

    public ErrorBody getError() {
        return error;
    }

    public void setError(ErrorBody error) {
        this.error = error;
    }

    public static class ErrorBody implements Serializable {

        private String message;
        private int code;

        public ErrorBody(){}

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
