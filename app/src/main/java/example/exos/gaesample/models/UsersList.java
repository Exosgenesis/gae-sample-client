package example.exos.gaesample.models;

import java.io.Serializable;
import java.util.Arrays;

public class UsersList implements Serializable {

    private User[] items;

    public UsersList() {
    }

    public User[] getItems() {
        return items;
    }

    public void setItems(User[] items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "UsersList{" +
                "items=" + Arrays.toString(items) +
                '}';
    }
}
