package example.exos.gaesample.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.text.SimpleDateFormat;
import java.util.Date;

import example.exos.gaesample.MainActivity;
import example.exos.gaesample.R;
import example.exos.gaesample.base.BaseApp;

public class MyGcmListenerService extends GcmListenerService {

//    private static final String TAG = "MyGcmListenerService";
    private static final String TAG = BaseApp.TAG;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Long timestamp = Long.valueOf(data.getString("timestamp"));
        String title = data.getString("title");

        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Title: " + title);
        Log.d(TAG, "TimeStamp: " + timestamp);

        sendNotification(title, timestamp);
    }

    private String formatDate(long timestamp) {
        Date date = new Date(timestamp);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        return format.format(date) + " (" + timestamp + ')';
    }

    private void sendNotification(String title, long timestamp) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(formatDate(timestamp))
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}