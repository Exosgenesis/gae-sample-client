package example.exos.gaesample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import example.exos.gaesample.R;
import example.exos.gaesample.models.User;

/**
 * Created by Exos on 12.09.2015.
 */
public class UserListAdapter extends BaseAdapter {

    private User[] users;
    private LayoutInflater inflater;

    public UserListAdapter(User[] users, Context context) {
        this.users = users;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return users.length;
    }

    @Override
    public User getItem(int i) {
        return users[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = inflater.inflate(R.layout.user_item, null, false);

        User u = getItem(i);
        setText(view, R.id.user_id, u.getId());
        setText(view, R.id.user_name, u.getName());

        return view;
    }

    private void setText(View v, int resId, Object o) {
        if (o == null || String.valueOf(o).trim().equals(""))
            return;
        ((TextView)v.findViewById(resId)).setText(String.valueOf(o));
    }
}
