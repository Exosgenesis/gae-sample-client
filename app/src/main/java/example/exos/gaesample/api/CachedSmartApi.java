package example.exos.gaesample.api;

import java.io.Serializable;
import java.util.HashMap;

import example.exos.gaesample.models.GcmSenderTarget;
import example.exos.gaesample.models.User;
import example.exos.gaesample.models.UsersList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Path;

/**
 * Не дает "прострелить себе ногу" - при частой смене вьюшек наплодить 1000 запросов.
 */
public class CachedSmartApi extends CachedApi {

    private final static String USER_KEY = "user";
    private final static String USERS_KEY = "users";
    private final static String GCM_KEY = "gcm";

    private HashMap<String, PendingRequest> pendings = new HashMap<>();

    public CachedSmartApi(Api api) {
        super(api);
    }

    @Override
    public void getUsers(Callback<UsersList> cb) {
        if (!swapCallback(cb, USERS_KEY))
            super.getUsers(new ProxyCallback<>(USERS_KEY));
    }

    @Override
    public void getUser(@Path("id") Long id, Callback<User> cb) {
        if (!swapCallback(cb, USER_KEY + id))
            super.getUser(id, new ProxyCallback<>(USER_KEY +id));
    }

    @Override
    public void gcmTest(@Body GcmSenderTarget target, Callback<Void> cb) {
        if (!swapCallback(cb, GCM_KEY))
            super.gcmTest(target, new ProxyCallback<>(GCM_KEY));
    }

    private<T> boolean swapCallback(Callback<T> cb, String key) {
        if (pendings.containsKey(key)) {
            PendingRequest r = pendings.get(key);
            r.cb = cb;
            return true;
        }

        PendingRequest<T> r = new PendingRequest<>(key, cb);
        pendings.put(r.uId, r);
        return false;
    }

    private static class PendingRequest<T> {
        String uId;
        Callback<T> cb;

        public PendingRequest(String uId, Callback<T> callback) {
            this.uId = uId;
            this.cb = callback;
        }
    }

    private class ProxyCallback<T> implements Callback<T> {

        private String key;

        public ProxyCallback(String key) {
            this.key = key;
        }

        @Override
        public void success(T res, Response response) {
            Callback<T> cb = pendings.get(key).cb;
            pendings.remove(key);
            cb.success(res, response);
        }

        @Override
        public void failure(RetrofitError error) {
            Callback<T> cb = pendings.get(key).cb;
            pendings.remove(key);
            cb.failure(error);
        }
    }
}
