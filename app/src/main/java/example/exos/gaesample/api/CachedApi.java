package example.exos.gaesample.api;

import java.util.HashMap;

import example.exos.gaesample.models.GcmSenderTarget;
import example.exos.gaesample.models.User;
import example.exos.gaesample.models.UsersList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public class CachedApi implements Api {

    private Api api;

    private HashMap<Long, User> users = new HashMap<>();
    private UsersList usersList;

    public CachedApi(Api api) {
        this.api = api;
    }

    @Override
    @GET("/_ah/api/sample/v1/user")
    public void getUsers(Callback<UsersList> cb) {
        if (usersList != null) {
            cb.success(usersList, null);
            return;
        }

        api.getUsers(new Callback<UsersList>() {
            @Override
            public void success(UsersList list, Response response) {
                usersList = list;
                cb.success(list, response);
            }

            @Override
            public void failure(RetrofitError error) {
                cb.failure(error);
            }
        });
    }

    @Override
    @GET("/_ah/api/sample/v1/user/{id}")
    public void getUser(@Path("id") Long id, Callback<User> cb) {
        if (users.get(id) != null) {
            cb.success(users.get(id), null);
            return;
        }

        api.getUser(id, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                users.put(user.getId(), user);
                cb.success(user, response);
            }

            @Override
            public void failure(RetrofitError error) {
                cb.failure(error);
            }
        });
    }

    @Override
    @POST("/_ah/api/sample/v1/gcm_sender")
    public void gcmTest(@Body GcmSenderTarget target, Callback<Void> cb) {
        api.gcmTest(target, cb);
    }

    public void clearCache() {
        users.clear();
        usersList = null;
    }
}
