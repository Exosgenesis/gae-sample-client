package example.exos.gaesample.api;

import example.exos.gaesample.models.GcmSenderTarget;
import example.exos.gaesample.models.User;
import example.exos.gaesample.models.UsersList;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface Api {
    @GET("/_ah/api/sample/v1/user")
    void getUsers(Callback<UsersList> cb);

    @GET("/_ah/api/sample/v1/user/{id}")
    void getUser(@Path("id") Long id, Callback<User> cb);

    @POST("/_ah/api/sample/v1/gcm_sender")
    void gcmTest(@Body GcmSenderTarget target, Callback<Void> cb);
}
