package example.exos.gaesample.base;

import android.app.Application;
import android.util.Log;

import example.exos.gaesample.api.Api;
import example.exos.gaesample.api.CachedApi;
import example.exos.gaesample.api.CachedSmartApi;
import retrofit.RestAdapter;

public class BaseApp extends Application {

    public final static String TAG = "GAE Sample";
    //local host for Genymotion emulator
    private final static String API_HOST = "10.0.3.2";
    private final static int API_PORT = 8080;

    private static Api api;

    public static Api getApi() {
        return api;
    }

    public static void logI(String msg) {
        Log.i(TAG, msg);
    }

    public static void logE(String msg) {
        Log.e(TAG, msg);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApi();
    }

    private void initApi() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(String.format("http://%s:%d", API_HOST, API_PORT))
                .build();
        api = new CachedSmartApi(restAdapter.create(Api.class));
    }
}
