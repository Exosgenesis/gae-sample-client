package example.exos.gaesample;

import android.os.Bundle;

public interface FragmentSelector {
    void selectFragment(int num, Bundle args);
}
